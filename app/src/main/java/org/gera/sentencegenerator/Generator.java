package org.gera.sentencegenerator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import android.util.Log;

import com.google.gson.Gson;

/**
 * Created by Herman on 03.10.2014.
 */
public class Generator {
    private Words words = new Words();
    private Random rand;
    private Gson gson = new Gson();

    public Generator() {rand = new Random();}

    public String getSentence() {
        StringBuilder sb = new StringBuilder();
//        Log.d("Checks", "list1.size() = "+list1.size());
//        Log.d("Checks", "list2.size() = "+list2.size());
//        Log.d("Checks", "list3.size() = "+list3.size());
        sb.append(words.getList1().get(rand.nextInt(words.getList1().size())));
        sb.append(" ");
        sb.append(words.getList2().get(rand.nextInt(words.getList2().size())));
        sb.append(" ");
        sb.append(words.getList3().get(rand.nextInt(words.getList3().size())));
        return sb.toString();

    }

    public String generateJSON()   {
        //return gson.toJson(words);
        return gson.toJson(new Words());
    }

    public void loadJSON(String json)  {
        words = gson.fromJson(json, words.getClass());
        //Log.d("Checks", words.getList1().get(3));
    }

    public static void main(String[] args) {
        Generator gen = new Generator();
    }
}
