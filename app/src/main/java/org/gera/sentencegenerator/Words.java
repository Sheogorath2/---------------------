package org.gera.sentencegenerator;

import java.util.ArrayList;

/**
 * Created by Herman on 03.10.2014.
 */
public class Words {
    private static String[] w1 = {"cвинья","собака","кошка","избушка","бабка","крот","петух","слон","луна","коза",
            "мошка","мышка","таракан запечный","желтый большой мяч","курица","панда","лев","бегемот","бабочка", "булочка", "жук","светлячок","ерш","кузнечик","стрекоза",
            "змея","ящерица","дядька ЗыЗ","улитка","хамелеон","трансформер", "стиралка", "дверь", "принтер","паровоз","земля","лес","море","гора","телевизор",
            "провод","подъемный кран","сумка","червяк","выхухоль","дикобраз","землекоп","зайчик","тысяча чертей",
            "черпак","милиционер","Бармаглот","лягушка-путешественница","Алиса","Баба-Яга","зомби","Пафнутий","бурундучок","Джерри","Змей-Горыныч","Протосс",
            "депутат Неколай Ильич","Губка Боб","Неприятный тип","Рапунцель","Шайтан-Арба", /*НОВЫЕ:*/ "сурок", "молодой человек",
            "Масяня", "хомячок", "высококвалифицированный сотрудник", "посторонний", "игрок", "индюк", "котиный мех", "неизвестый старичок",
            "защитник Отечества", "Шалтай-Болтай", "странное существо", "робот Бендер", "Незнайка", "ктото очень большой",
            "больной", "мастер Йода", "Иван Царевич", "человек-паук", "коммандир отряда", "чемпион", "доктор", "мелочь",
            "дворник", "капитан Джек Воробей", "бэтмэн", "человек-амфибия", "рак", "Смусмумрик", "принцесса на горошине", "Белоснежка",
            "белый кролик", "килька в томатном соусе", "попугай", "муравьед", "людоедоед", "длиннокот", "килограм колбасы",
            "десять негрятят", "авоська с хлебом", "старужка с авоськой", "драное пальто", "перо павлина", "рабочие штаны",
            "летающий макоронный монстр", "кот Грампи", "дохлый пират", "поклонник продукции apple", "уховертка", "упрямый подросток",
            "человек без мозгов", "морковевед", "пекинская капуста", "банановая шкурка", "шерочка с машерочкой", "мужик", "собака-исчейка",
            "палочка-выручалочка", "боевой слон", "кот Базилио", "мелкий рогатый скот", "волшебный кролик", "шестерукий Серафим",
            "новый пиджак", "мясной фарш", "карлик", "болотная выпь", "волчишко", "зеленый свин", "моя бабушка"};
    private static String[] w2 = {"хрюкает","понимает","мяукает","ходит на курьих ножках","злится","роет ямы","кричит","моется","отражает свет","спит",
            "купается","ворует зерно","не дохнет","прыгает","несет яйца","ест бамбук","рычит","моет рот","порхает",
            "жуется","копается","ярко светится","глупо прыгает","ловится","как вертолет","шипит как шипучка","пищит","как дядька ЗыЗ","медлит","светится",
            "топочет как слон","бешено рычит","тупо молчит","расцветает по утрам","считает себя выше других","гниет","тухнет",
            "прячется","пылится","дрыгает ногой","распевает песни","весело живет","кушает компот","издалека похоже на муху","получает медаль",
            "роется в карманах","свистит соловьем","сосет сушку","сушит сухари","ловит крабов","разводит грязь","отвечает на вопросы зрителей","передает привет маме"
            ,"стоит на голове","лязгает зубами","считает галок","обсуждает новости","красуется перед зеркалом","летит на марс","починяет примуса",
            "вычесывает блох","катится с горки","тушится в скороварке","жмурится и моргает","глядит сычом","сажает корнеплоды","копает чорную редьку",
            "произносит тост","за шаг от победы","почти добегает","немного не допрыгивает","медленно закипает","разлетается на куски","сипит","делает по своему",
            "странно себя ведет","отмечает юбилей","не может остановиться","гадает по звездам","не работает без батареек","подпрыгивает","растет над собой",
            "ленится помыть голову","кричит нечеловеческим голосом","неоднократно переспрашивает","принимает экзамен","проверяет талончики","ни разу не промахивается",
            "расцветает","внезапно ускоряется","становится героем","ждет автобуса","шлет СМСки","умничает","дурачится","ревет медведем","покупает молоко",
            "давится сухарями","мечтает о повышении","жмет на газ", /*НОВЫЕ:*/ "прыгает через веревочку", "летит с восьмого этажа", "подгорает",
            "входит в комнату", "играет в майнкрафт", "прогуливается по двору", "выпивает", "изменяет направление", "тыкает на кнопку",
            "говорит по скайпу", "чистит унитаз", "моет пол", "рисует портрет", "исполняет желание", "ждет деда мороза", "не прислоняется",
            "пересекается", "недоумевает", "размораживает холодильник", "бежит на остановку", "гадит", "высчитывает прибыль", "выносит мозги",
            "убивает всех людей", "проводит вечер", "изменяется", "думает над своими поступками", "называет себя квалифицированным сотрудником",
            "заканчивает жизнь", "смотрит телевизор", "разрабатывает под андроид", "клепает гостивухи", "строит космическую станцию",
            "является членом ООО", "побивает рекорд по количеству прожитых дней", "выигрывает чемпионат мира", "грабит корованы",
            "тратит все свои деньги", "копает корнеплоды", "идет в школу", "делает уроки", "стреляет из автомата", "моет руки",
            "страдает от изжоги", "заливает бетоном площатку", "регулирует движение", "неопределенно хмыкает", "сортирует  бумажки",
            "протирает глаза", "протирает штаны", "выступает в цирке с конями", "жанглирует бутылками", "продает водку", "покупает водку",
            "пьет водку", "хлопнул коньяку","заблудился в тумане", "находится на грани", "путешествует", "питается съедобными корнями",
            "обьедает молодые побегы", "поет и пляшет", "выступает с оркестром", "чешется боком", "чешется об столб", "клюет зернышки",
            "сматывает удочки", "накупил китайской лапши", "купил китайскую куртку", "лежит на диване", "режет арбуз", "кует сталь",
            "чешет лед", "фонтанирует идеями", "исполнен оптимизма", "потерял последнюю надежду", "проявляет чудеса храбрости",
            "застрял и не может вылести", "сидит уже полтора года", "учит ноты", "бренчит на кселофоне","изучает морковь", "смотрит сериал",
            "вне себя", "родился в сорочке", "с писаной торбой", "грошит огурцы", "чистит сопоги", "уезжает в тьму-Таракань",
            "убивает двух зайцев", "точит лясы", "строит воздушные замки", "стоит как вкопанный", "рассказывает сказку про белого бычка",
            "с боку припека", "оброс плесенью", "прилипла как муха", "стесняется говорить", "движется равномерно и прямолинейно",
            "едет по рельсам", "является центром Вселенной", "мечтает о будущем", "планирует покупки", "продает шкуру неубитого медведя",
            "проваливается сквозь землю", "передает эстафету", "посыпает голову пеплом", "попадает в беду", "кладет в долгий ящик",
            "подкладывает свинью", "существует", "ищет работу", "перегоняет стада", "собирается с силами", "пляшет под будкой",
            "вещает нос на квинту", "сильно грустит", "перемывает косточки", "переливает из пустого в порожнее", "ест калачи",
            "ни бельмеса не понимает", "на ходу подметки рвет", "опаздывает", "мутит воду", "морочит голову",
            "важнечает", "льет крокодиловы слезы", "идет ва-банк", "бьет баклуши", "прилюдно выражается", "копит денежки",
            "лежит неподвижно", "смотрит с высока", "вращается", "оссознает ответственнось", "осознает свое предназначение",
            "калышется на ветру", "курит трубку", "пускает пар", "чешит пузико", "повелевает вселенной", "набирает опыт"};
    private static String[] w3 = {"в грязи","у крыльца","под ногами","по земле","над кроватью","в яме","на заборе","над рекой","под солнцем","на лугу",
            "в варенье","в клетке","под печью","в подвале","в хлеву","среди звезд","в саванне","у дяди Пети","в хлебнице","у колонки"
            ,"в коробочке","на поляне","в пруду","в поле", "внутри","в норе змеи","в ящике","в дядьке ЗыЗе","в консервной банке","под пионами"
            ,"под шафе","ни о чем","бегемотиком","как курица","в пыли","через голову","кракозяброй","среди овец","по микрофону","и немедленно просыпается"
            ,"на антресолях","навсегда","недовольно и свирепо, но в тоже время грустно и с недоумением","звонко лапами бренча",
            "за отличную службу","в мешке для мусора","в грязном чулане","в изоляторе для больных","на жердочке","в свободном полете"
            ,"на тонкой веревочке","под балконом","возле остановки","под чупа-чупсом","на станции метро Купаловская","в Гамбурге","калачиком",
            "сидя на табуретке","ни в чем не сомневаясь","и после этого еще чего-то хочет","каким-то образом","очень весело","весело улыбаясь",
            "разевая рот","постукивая по столу","не замечая ничего вокруг","автоматически","очень старательно","в начищенных сапогах",
            "фактически в одиночку","терзаясь сомнениями","неопределенно подмигивая","разгребая мусор","в Эрмитаже","у бабушки за печкою",
            "у фонтана",", а еще он свет не выключает",", но это еще ни о чем не говорит",", почему бы и нет, в самом деле","на самом деле",
            "весело и гордо","активно питаясь","никому ничего не говоря","разворачиваясь всем корпусом","балансируя на веревке","не заполнив дневника",
            "медленно и методично","ласточкой","рябчиком","не снижая скорости","внутренне содрогаясь","о чем-то думая","непонятно зачем","без приказа",
            /*НОВЫЕ:*/ "незамедлительно", "без вопросов", "непонятно зачем", "как и обещал", "распушив хвост", "как терминатор", "несмотря ни на что",
            "пружиня на ногах", "нечем не занимаясь", "опаздывая на автобус", "под дверью", ", и почему это смешно???", "постоянна задавая себе вопросы",
            "интересуясь происходящим", "со скоростью света", "под парашютом", "ожидая поезда", "и получает подарки", "защищая Украину",
            "таковым не являясь", "и это очень круто", "минуту назад", "выдыхая воздух", "убегая от преследования", "в кино",
            "находясь на работе", "с грязными волосами", "ведь верно? ведь правильно?", "под присмотром", "в сарае",
            "на параде", "на шести ногах", "за пяттнадцать рублей","передом", "задом", "задомнаперед", "бесплатно", "и чувствует себя замечательно",
            "хотя и крайне недовольно", "и чувствует себя неплохо", "но качество ужасное", "зато можно стирать", "и уже разучился удивлятся",
            "об забор", "вверх ногами", "с широко раскрытыми глазами", "от балды", "подручными средствами", "с помощью кирпича",
            "на скатерть", "за окном", "перед дверями", "выше крыши", "заедая все это морковкой", "за стеклом",
            "в стеклянном ящике", "в почтовом ящике", "ни чему не удивляясь", "громко хлопая крыльями", "громко хлопая", "вчера вечером",
            "без царя в голове", "в мясорубке", "вертит котлеты", "хоть лоgyи", "тытелька в тютельку", "тихой спой", "спустя рукава",
            "с бухты-борахты", "без шипов", "на немытой тарелки", "за высоким забором", "на квадратных колесах", "посреди города", "на витрине",
            "как кур во щи", "после дождечка в четверг", "на подножном корму","повесивши нос", "по горячим следам", "огнем и мечом",
            "не в своей тарелке", "межку Сциллой и Хариблой", "как за каменной стеной", "не солонахлебавши"};

    private ArrayList<String> list1 = new ArrayList<String>();
    private ArrayList<String> list2 = new ArrayList<String>();
    private ArrayList<String> list3 = new ArrayList<String>();

    public Words()  {
//        Log.d("Checks1", "list1.size() = "+list1.size());
//        Log.d("Checks1", "list2.size() = "+list2.size());
//        Log.d("Checks1", "list3.size() = "+list3.size());

        for(String s: w1) list1.add(s);
        for(String s: w2) list2.add(s);
        for(String s: w3) list3.add(s);

//        Log.d("Checks2", "list1.size() = "+list1.size());
//        Log.d("Checks2", "list2.size() = "+list2.size());
//        Log.d("Checks2", "list3.size() = "+list3.size());
    }

    public ArrayList<String> getList1() {
        return list1;
    }

    public void setList1(ArrayList<String> list1) {
        this.list1 = list1;
    }

    public ArrayList<String> getList2() {
        return list2;
    }

    public void setList2(ArrayList<String> list2) {
        this.list2 = list2;
    }

    public ArrayList<String> getList3() {
        return list3;
    }

    public void setList3(ArrayList<String> list3) {
        this.list3 = list3;
    }
}
